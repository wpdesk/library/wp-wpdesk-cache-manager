<?php

namespace WPDesk\CacheManager;

class CacheManager {
    const TIMEOUT_SUFFIX = '_timeout';
    const AUTOLOAD_VALUE = 'no';

    /**
     * @var ObjectDataInterface
     */
    private $object_data;

    /**
     * @var string
     */
    private $transient_name;

    /**
     * @var int
     */
    private $transient_timeout;

    /**
     * @param ObjectDataInterface $object_data .
     * @param string|null         $transient_name .
     * @param int                 $transient_timeout .
     */
    public function __construct( ObjectDataInterface $object_data, string $transient_name = null, int $transient_timeout = 86400 ) {
        $this->object_data       = $object_data;
        $this->transient_name    = $transient_name ?: get_class( $object_data );
        $this->transient_timeout = $transient_timeout;
    }

    /**
     * @return mixed
     */
    public function get_data( bool $force = false, $default_value = null ) {
        $cached_data = $this->get_cached_data();

        if ( ! $force && $cached_data !== null && ! $this->is_expired() ) {
            return $cached_data;
        }

        try {
            return $this->set_cached_data( $this->object_data->get() );
        } catch ( ExceptionGetData $e ) {
            if ( $force ) {
                throw $e;
            }
        }

        return $cached_data ?: $default_value;
    }

    /**
     * @return void
     */
    public function clear_cache() {
        delete_option( $this->transient_name );
        delete_option( $this->transient_name . self::TIMEOUT_SUFFIX );
    }

    /**
     * @return int
     * @codeCoverageIgnore
     */
    protected function get_current_time() {
        return time();
    }

    /**
     * @return bool
     */
    private function is_expired(): bool {
        $expiration = (int) get_option( $this->transient_name . self::TIMEOUT_SUFFIX, 0 );

        return $this->get_current_time() > $expiration;
    }

    /**
     * @return mixed
     */
    private function get_cached_data() {
        return get_option( $this->transient_name, null );
    }

    /**
     * @param mixed $cached_data .
     *
     * @return mixed
     */
    private function set_cached_data( $cached_data ) {
        update_option( $this->transient_name, $cached_data, self::AUTOLOAD_VALUE );
        update_option( $this->transient_name . self::TIMEOUT_SUFFIX, $this->get_current_time() + $this->transient_timeout, self::AUTOLOAD_VALUE );

        return $cached_data;
    }
}
