<?php

namespace WPDesk\CacheManager;

interface ObjectDataInterface {
	/**
	 * @return mixed
	 * @throws ExceptionGetData
	 */
	public function get();
}
