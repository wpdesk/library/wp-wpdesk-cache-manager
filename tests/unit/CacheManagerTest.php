<?php

namespace unit;

use PHPUnit\Framework\MockObject\MockObject;
use WP_Mock;
use WP_Mock\Tools\TestCase;
use WPDesk\CacheManager\CacheManager;
use WPDesk\CacheManager\ExceptionGetData;
use WPDesk\CacheManager\ObjectDataInterface;

class CacheManagerTest extends TestCase {
    /**
     * @var MockObject|ObjectDataInterface
     */
    private $object_data;
    /**
     * @var MockObject|ObjectDataInterface
     */
    private $cache_manager_under_test;

    public function setUp() {
        WP_Mock::setUp();

        $this->object_data = $this->getMockBuilder( ObjectDataInterface::class )->setMethods( [ 'get' ] )->getMock();

        $this->cache_manager_under_test = $this->getMockBuilder( CacheManager::class )
                                               ->setConstructorArgs( [ $this->object_data, 'cache_name' ] )
                                               ->setMethods( [ 'get_current_time' ] )->getMock();
    }

    public function tearDown() {
        WP_Mock::tearDown();
    }

    public function testShouldThrowExceptionIfObjectDataThrowException() {
        // Except.
        $exception = new ExceptionGetData;

        $this->expectException( ExceptionGetData::class );
        $this->object_data->expects( self::once() )->method( 'get' )->willThrowException( $exception );

        WP_Mock::userFunction( 'get_option' )->once()->withArgs( [ 'cache_name', null ] )->andReturnNull();


        // Given.

        // When.
        $actual = $this->cache_manager_under_test->get_data( true );

        // Then.
        $this->assertTrue( true );
    }

    public function testShouldGetAndSaveIfCacheIsNull() {
        // Except.
        $expected = 'data to cache';

        $this->object_data->expects( self::once() )->method( 'get' )->willReturn( $expected );
        $this->cache_manager_under_test->expects( self::once() )->method( 'get_current_time' )->willReturn( 1 );

        WP_Mock::userFunction( 'get_option' )->once()->withArgs( [ 'cache_name', null ] )->andReturnNull();
        WP_Mock::userFunction( 'update_option' )->once()->with( 'cache_name', $expected, 'no' );
        WP_Mock::userFunction( 'update_option' )->once()->with( 'cache_name_timeout', 86401, 'no' );

        // When.
        $actual = $this->cache_manager_under_test->get_data();

        // Then.
        $this->assertEquals( $expected, $actual );
    }

    public function testShouldGetSavedDataIfNotExpired() {
        // Except.
        $expected = 'cached data';

//        $this->object_data->expects( self::once() )->method( 'get' )->willReturn( $expected );
        //$this->cache_manager_under_test->expects( self::once() )->method( 'get_current_time' )->willReturn( 1 );

        WP_Mock::userFunction( 'get_option' )->once()->with( 'cache_name', null )->andReturn( $expected );
        WP_Mock::userFunction( 'get_option' )->once()->with( 'cache_name_timeout', 0 )->andReturn( 2 );
//        WP_Mock::userFunction( 'update_option' )->once()->with( 'cache_name', $expected, 'no' );
//        WP_Mock::userFunction( 'update_option' )->once()->with( 'cache_name_timeout', 86401, 'no' );

        // When.
        $actual = $this->cache_manager_under_test->get_data();

        // Then.
        $this->assertEquals( $expected, $actual );
    }

    public function testShouldGetSavedDataIfExpiredAndThrowNewException() {
        // Except.
        $expected = 'cached data';

        $exception = new ExceptionGetData;
        $this->object_data->expects( self::once() )->method( 'get' )->willThrowException( $exception );

        $this->cache_manager_under_test->expects( self::once() )->method( 'get_current_time' )->willReturn( 1 );

        WP_Mock::userFunction( 'get_option' )->once()->with( 'cache_name', null )->andReturn( $expected );
        WP_Mock::userFunction( 'get_option' )->once()->with( 'cache_name_timeout', 0 )->andReturn( 0 );

        // When.
        $actual = $this->cache_manager_under_test->get_data();

        // Then.
        $this->assertEquals( $expected, $actual );
    }

    public function testShouldGetDefaultValueIfThrowNewException() {
        // Except.
        $exception = new ExceptionGetData;
        $this->object_data->expects( self::once() )->method( 'get' )->willThrowException( $exception );

        WP_Mock::userFunction( 'get_option' )->once()->with( 'cache_name', null )->andReturnNull();

        // When.
        $actual = $this->cache_manager_under_test->get_data( false, 'default_value' );

        // Then.
        $this->assertEquals( 'default_value', $actual );
    }

    public function testShouldClearCache() {
        // Except.
        WP_Mock::userFunction( 'delete_option' )->once()->with( 'cache_name' );
        WP_Mock::userFunction( 'delete_option' )->once()->with( 'cache_name_timeout' );

        // When.
        $this->cache_manager_under_test->clear_cache();

        // Then.
        $this->assertTrue( true );
    }
}
